#' This script aims at performing a dotplot to study expression diff among clusters
#' Project: AZIMUT
#' Author: Nicolas Vallet

#' Require
library(ggplot2)
library(RColorBrewer)
library(tidyverse)
library(reshape2)

#' Set path
path_to_git    = "~/Git/azimutscrna"
path_to_save   = "~/tmp"

#' Get significant and FC abs>0.15 genes in cd4 and cd8 subsets
setwd(path_to_git)
cd4 = read_csv2("res/diffexpr_clustcd4.csv") %>%
    filter(p_val_adj < 0.05 & abs(avg_logFC)>0.15) %>%
    pull(...1)
cd8 = read_csv2("res/diffexpr_clustcd8.csv") %>%
    filter(p_val_adj < 0.05 & abs(avg_logFC)>0.15) %>%
    pull(...1)
genes = unique(c(cd4, cd8)) # merge genes and remove duplicates
rm(cd4,cd8)

#' Import datasets with results of individual clusters
clust0 = read_csv2("res/diffexpr_clust0.csv") %>%
    add_column(cluster = "Clust0") %>%
    filter(...1 %in% genes)
clust1 = read_csv2("res/diffexpr_clust1.csv") %>%
    add_column(cluster = "Clust1") %>%
    filter(...1 %in% genes)
clust3 = read_csv2("res/diffexpr_clust3.csv") %>%
    add_column(cluster = "Clust3") %>%
    filter(...1 %in% genes)
clust5 = read_csv2("res/diffexpr_clust5.csv") %>%
    add_column(cluster = "Clust5") %>%
    filter(...1 %in% genes)
clust7 = read_csv2("res/diffexpr_clust7.csv") %>%
    add_column(cluster = "Clust7") %>%
    filter(...1 %in% genes)
clust8 = read_csv2("res/diffexpr_clust8.csv") %>%
    add_column(cluster = "Clust8") %>%
    filter(...1 %in% genes)
clust9 = read_csv2("res/diffexpr_clust9.csv") %>%
    add_column(cluster = "Clust9") %>%
    filter(...1 %in% genes)
clust12 = read_csv2("res/diffexpr_clust12.csv") %>%
    add_column(cluster = "Clust12") %>%
    filter(...1 %in% genes)
clust15 = read_csv2("res/diffexpr_clust15.csv") %>%
    add_column(cluster = "Clust15") %>%
    filter(...1 %in% genes)
clust17 = read_csv2("res/diffexpr_clust17.csv") %>%
    add_column(cluster = "Clust17") %>%
    filter(...1 %in% genes)
clust19 = read_csv2("res/diffexpr_clust19.csv") %>%
    add_column(cluster = "Clust19") %>%
    filter(...1 %in% genes)
clust25 = read_csv2("res/diffexpr_clust25.csv") %>%
    add_column(cluster = "Clust25") %>%
    filter(...1 %in% genes)

#' merge all and scale the dataframe for the dotplot
## merge dataframe and apply FC sign and absolute FC for scaling
data = rbind(clust0, clust1, clust3, clust5, clust7, clust8, clust9, clust12, clust15, clust17, clust19, clust25) %>%
    filter(p_val_adj < 0.05) %>%
    rename(gene = ...1) %>%
    select( -c(pct.1, pct.2, p_val) ) %>%
    mutate(fc  = abs(avg_logFC),
           sign = ifelse(avg_logFC<0,
                        "lower",
                        "higher")) %>%
    ## will be used to merge scaled dataframe with the raw dataframe
    mutate(merge_col = paste(gene, cluster, sep = "_")) 

## perform the scaling to maximum (MAX = 1 and other columns are ratio of the max)
cast = data %>%
    #' transform to dataframe with columns = genes to perform the scaling
    dcast(cluster ~ gene, value.var = "fc") %>%
    column_to_rownames("cluster") %>%
    #' scale to max
    apply(MARGIN = 2, FUN = function(X) (X/max(X,na.rm = TRUE)) ) %>%
    as.data.frame() %>%
    #' retransform to melt dataframe, create the merging var with gene_cluster
    rownames_to_column("cluster") %>%
    melt(id.vars = "cluster") %>%
    rename(gene = variable) %>%
    mutate(merge_col = paste(gene, cluster, sep = "_")) %>%
    select(-c(gene, cluster)) %>%
    as_tibble() %>%
    inner_join(data, by = "merge_col") %>%
    select(-merge_col) %>%
    rename(fc_scaled = value) %>%
    #' reorder cluster names
    mutate(cluster = fct_relevel(cluster,
                                 "Clust25", "Clust12", "Clust8", "Clust17", "Clust7", "Clust19", "Clust3", "Clust5", "Clust15", "Clust9", "Clust1", "Clust0"
                                 ))

#' Get ordered gene values according to the mean FC among all clusters
meanfc = cast %>%
    group_by(gene) %>%
    summarize(Mean = mean(avg_logFC, na.rm=TRUE) ) %>%
    mutate(gene = fct_reorder(gene, desc(-Mean)))
ordered = levels(meanfc$gene)

cast = cast %>%
    mutate(gene = fct_relevel(gene, ordered))

meanfc = meanfc %>% #' transform meanfc to be added to the dotplot
    rename(avg_logFC = Mean) %>%
    mutate(fc  = abs(avg_logFC),
           sign = ifelse(avg_logFC<0,
                        "lower",
                        "higher")) %>%
    add_column(fc_scaled = NA, cluster = "Mean", p_val_adj = NA)

cast = cast %>% rbind(meanfc) %>%
    mutate(cluster = fct_relevel(cluster, after = Inf))

#' Draw the plots
theme = list(
    theme_minimal(),
    theme(axis.text.x = element_text(angle=45, hjust = 0.95),
          axis.text   = element_text(face = "bold", color = "black"),
          axis.title  = element_blank(),
          axis.line   = element_line(linetype = "solid", color = "black"),
          axis.ticks  = element_line(linetype = "solid", color = "black"),
          ))
scaled = cast %>%
    ggplot(aes(x = gene, y = cluster, size = fc_scaled, color = sign)) +
    geom_point(shape= 16) +
    ggtitle("scaled fold change")+
    scale_color_manual(values = c("#d7191c", "#2c7bb6"))+
    theme
raw = cast %>%
    ggplot(aes(x = gene, y = cluster, size = fc, color = sign)) +
    geom_point(shape= 16) +
    ggtitle("fold change") +
    scale_color_manual(values = c("#d7191c", "#2c7bb6"))+
    theme

#' Export to pdf
setwd(path_to_save)
pdf("dotplot_geneexpr.pdf", width = 9, height = 5)
print(scaled)
print(raw)
dev.off()
