#' Volcano plots
#' Project: AZIMUT
#' Author: Nicolas Vallet

#' Require
library(ggplot2)
library(ggrepel)
library(RColorBrewer)
library(tidyverse)

#' Set path
path_to_git  = "~/Git/azimutscrna/"
path_to_save = "~/tmp" 

#' Load
setwd(path_to_git)
cd4 = read.csv2("res/diffexpr_clustcd4.csv")
cd8 = read.csv2("res/diffexpr_clustcd8.csv")

#' pval = 0 are indexed as 1/10 of lowest pval
cd4$p_val_adj =  ifelse(cd4$p_val_adj == 0,
                         0.1*unique(cd4$p_val_adj)[2],
                        cd4$p_val_adj)
cd8$p_val_adj =  ifelse(cd8$p_val_adj == 0,
                         0.1*unique(cd8$p_val_adj)[2],
                         cd8$p_val_adj)

#' draw the plot

cd4plot = ggplot() +
    geom_point(data=cd4, aes(x = avg_logFC, y = -log(p_val_adj,10) ) ) +
    geom_point(data = cd4 %>% filter(p_val_adj < 0.05 & abs(avg_logFC) > (0.15)),
               aes(x = avg_logFC, y = -log(p_val_adj,10) ),
               shape = 16, color = "#E31A1C", size = 3) +
    geom_text_repel(data = cd4 %>% filter(p_val_adj < 0.05 & abs(avg_logFC) > (0.15)),
                    aes(x = avg_logFC, y = -log(p_val_adj,10), label = X ) ) +
    geom_hline(yintercept=-log(0.05,10), linetype = "dotted")+
    geom_vline(xintercept=0.15, linetype = "dotted") +
    geom_vline(xintercept=0, linetype = "dotted") +
    geom_vline(xintercept=-0.15, linetype = "dotted" )+
    xlab("Log2 fold change AZM/CTRL") +
    ylab("-Log10 adjusted p-value") +
    scale_x_continuous(limits = c(-0.35, 0.4), breaks=seq(-0.4, 0.4, by = 0.1 )) +
    scale_y_continuous(breaks=seq(0, 300, by = 50 )) +
    theme_classic() +
    theme(axis.text = element_text(color = "black", face = "bold", size = 12),
          axis.title = element_text(color = "black", face = "bold", size = 12),
          axis.line = element_line(color = "black"),
          panel.border = element_blank()
          )

cd8plot = ggplot() +
    geom_point(data=cd8, aes(x = avg_logFC, y = -log(p_val_adj,10) ) ) +
    geom_point(data = cd8 %>% filter(p_val_adj < 0.05 & abs(avg_logFC) > (0.15)),
               aes(x = avg_logFC, y = -log(p_val_adj,10) ),
               shape = 16, color = "#1F78D4",  size = 3) +
    geom_text_repel(data = cd8 %>% filter(p_val_adj < 0.05 & abs(avg_logFC) > (0.15)),
                    aes(x = avg_logFC, y = -log(p_val_adj,10), label = X ) ) +
    geom_hline(yintercept=-log(0.05,10), linetype = "dotted")+
    geom_vline(xintercept=0.15, linetype = "dotted") +
    geom_vline(xintercept=0, linetype = "dotted") +
    geom_vline(xintercept=-0.15, linetype = "dotted" )+
    xlab("Log2 fold change AZM/CTRL") +
    ylab("-Log10 adjusted p-value") +
    scale_x_continuous(limits = c(-0.35, 0.4),breaks=seq(-0.4, 0.4, by = 0.1 )) +
    scale_y_continuous(breaks=seq(0, 300, by = 50 )) +
    theme_classic() +
    theme(axis.text = element_text(color = "black", face = "bold", size = 12),
          axis.title = element_text(color = "black", face = "bold", size = 12),
          axis.line = element_line(color = "black"),
          panel.border = element_blank()
          )

setwd(path_to_save)
pdf("~/tmp/vpcd4cd8.pdf", width = 6, height = 4)
print(cd4plot)
print(cd8plot)
dev.off()
