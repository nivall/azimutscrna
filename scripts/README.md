Input files are obtained from 10X `cellranger` `count` then `aggr` pipeline. Figures and statistics outputs are generated in `~/tmp/` folder while `seurat` objects are stored in `.RData` format in `~/scrna_files`.

# Data processing and analyses

* Read aggregated `HDF5` file and perform data processing, output quality control results and stores a `.RData` file (`preprocess_aggr_outs.R`)
* Compare relative abundance of clusters between conditions (`diffrelab_adtclust.R`)
* Differential gene expression among clusters (`diffexpression_adtclust.R`)

# Figures 

* Draw boxplot of relative abundances of clusters in stimulated cells according to treatment (`boxplots_popstim_azmpla.R`)
* Draw dotplot of fold change gene expression in stimulated cells (`dotplot_genexpr_inclust.R`)
* Draw pie chart of number of cells in each conditions (`pie_chart.R`)
* Draw ridge plot of antibody derived tag channels (`ridgeplots_adt.R`)
* Draw UMAP from `.RData` file (`umap.R`)
* Draw volcano plots from differential gene expression in CD4+ and CD8+ clusters (`volcanoplots.R`)

