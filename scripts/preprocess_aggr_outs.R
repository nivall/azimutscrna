#' Preprocessing of CELLRANGER AGGR outputs with seurat
#' Project: AZIMUT
#' Author: Nicolas Vallet

#' Require
library(Seurat)
library(tidyverse)
library(patchwork)
library(ggplot2)
library(ComplexHeatmap)
library(RColorBrewer)

#' Choose data without 10X normalization (aggr output HDF5)
path_to_files = "~/scrna_files/azimutprolif_none"
setwd(path_to_files)

#' Read matrix files data
prolif.data = Read10X(data.dir = path_to_files)

#' Initialize Seurat object with the ouput from CELLRANGER AGGR pipeline no 10X normalization
## Input the Gene expression matrix
prolif = CreateSeuratObject( counts = prolif.data$`Gene Expression` )
## Add the Antibody capture matrix
prolif[["Protein"]] = CreateAssayObject( counts = prolif.data$`Antibody Capture` )
rm(prolif.data)

#' Apply cells metadata to seurat object
metadata_cells = read.csv("~/Git/azimutscrna/cellranger_aggr_scripts/aggregation.csv") %>%
    rownames_to_column("cell_n") %>%
    select( sample_id, condition, sample_names, cell_n ) %>%  # code 1 well by a number : cell_n (18A, 18D etc.) in the order by which data were merged (cf. aggregation.csv)
    left_join(
        as_tibble( Cells(prolif) ) %>% # get UMI and cell_n (well merged)
        separate(value, sep = "-", into = c("UMI", "cell_n"), remove = FALSE)) %>% 
    relocate(value)
prolif = AddMetaData(prolif, metadata = metadata_cells$condition, col.name = "condition")
prolif = AddMetaData(prolif, metadata = metadata_cells$sample_names, col.name = "sample_names")
prolif = AddMetaData(prolif, metadata = metadata_cells$sample_id, col.name = "sample_id")
rm(metadata_cells)

#' Quality control preprocess
## Get % of mitochondrial gene within each cell
prolif[["percent.mt"]] = PercentageFeatureSet( prolif, pattern = "^MT-" )
## Print QC Violin plots
pdf("~/tmp/qc_violin.pdf")
VlnPlot(prolif,
        pt.size = 0,
        features = c( "nFeature_RNA", "nCount_RNA", "percent.mt" ),
        ncol = 3) +
    theme(legend.position = "none")
VlnPlot(prolif,
        pt.size = 0.001,
        features = c( "nFeature_RNA", "nCount_RNA", "percent.mt" ),
        ncol = 3) +
    theme(legend.position = "none")
dev.off()

#' Visualize cutoffs for QC
## Define cutoffs
mt = 12.5
featureslow = 900
featureshigh = 6000
pdf("~/tmp/qc_violin_cutoff.pdf")
VlnPlot(prolif,
        pt.size = 0,
        features = "percent.mt" )+
    geom_hline(yintercept = mt) +
    ggtitle(paste0("cutoff=",mt)) +
    theme(legend.position = "none")
VlnPlot(prolif,
        pt.size = 0,
        features = "nFeature_RNA" ) +
    geom_hline(yintercept = featureslow) +
    geom_hline(yintercept = featureshigh) +
    ggtitle(paste0("cutoffs low=", featureslow, " & high=", featureshigh)) +
    theme(legend.position = "none")
dev.off()
## Print scatter plots for nCounts and mitochondrial and features
pdf("~/tmp/qc_scatter.pdf")
FeatureScatter(prolif, feature1 = "nCount_RNA", feature2 = "percent.mt") +
    geom_hline(yintercept = mt) +
    theme(legend.position = "none")
FeatureScatter(prolif, feature1 = "nCount_RNA", feature2 = "nFeature_RNA") +
    geom_hline(yintercept = featureshigh) +
    geom_hline(yintercept = featureslow) +
    theme(legend.position = "none")
dev.off()

#' Filter the unwanted events
## filter the object
prolif = subset(prolif, subset = nFeature_RNA > featureslow & nFeature_RNA < featureshigh & percent.mt < mt )
## Print QC post filter
pdf("~/tmp/qc_violin_filtered.pdf")
VlnPlot(prolif,
        pt.size = 0,
        features = c( "nFeature_RNA", "nCount_RNA", "percent.mt" ),
        ncol = 3) +
    theme(legend.position = "none")
VlnPlot(prolif,
        pt.size = 0.001,
        features = c( "nFeature_RNA", "nCount_RNA", "percent.mt" ),
        ncol = 3) +
    theme(legend.position = "none")
dev.off()
rm(featureshigh, featureslow, mt)


#' Normalizing
message("Log normalization")
prolif = NormalizeData(prolif, normalization.method = "LogNormalize", scale.factor = 10000)

#' Find high variable features
## Get the highly variable features
message("Get the 2000 highly variable features")
prolif = FindVariableFeatures(prolif, selection.method = "vst", nfeatures = 2000)
## plot these variables
plot1 = VariableFeaturePlot(prolif)
top30 = head(VariableFeatures(prolif), 30)
pdf("~/tmp/highvariable_features.pdf")
LabelPoints(plot = plot1, points = top30, repel = TRUE)
dev.off()

#' Scale data gene
all_genes = rownames(prolif)
prolif = ScaleData(prolif, features = all_genes)

#' Normalize and protein assay
DefaultAssay(prolif) = "Protein"
prolif = NormalizeData(prolif, normalization.method = "CLR", margin = 2, assay = "Protein")
ADT = rownames(prolif[["Protein"]])
prolif = ScaleData(prolif, feature = ADT)

#' Cluster on ADT
## get ADT names
prolif = RunPCA(prolif, features = ADT)
prolif = FindNeighbors(prolif, dims = 1:20)
prolif = FindClusters(prolif, resolution = 0.8)
prolif = RunUMAP(prolif, dims = 1:20)
## Print UMAP with clusters
pdf("~/tmp/umap_clusters.pdf", height = 10, width = 10)
DimPlot(prolif, reduction = "umap", label = TRUE) + NoLegend()
dev.off()
## Print UMAP according to groups
pdf("~/tmp/umap_grp.pdf", height = 5, width = 15)
DimPlot(prolif, reduction = "umap", split.by = "condition")
dev.off()
## Print UMAP according to ADT
pdf("~/tmp/umap_feat.pdf", height = 20, width = 20)
FeaturePlot(prolif, features = ADT)
dev.off()

#' Get heatmap of scaled average expression of ADT by clusters
## extract expression matrix
Idents(prolif) = prolif@meta.data$seurat_clusters
exp_mat = AverageExpression(prolif,
                            assays = "Protein",
                            features = ADT,
                            group.by = "ident")
exp_mat = as.data.frame(exp_mat$Protein)
names(exp_mat) = paste0("Cluster ", names(exp_mat))
row.names(exp_mat) = gsub( "-TotalSeqB", "", row.names(exp_mat) )
## extract cluster abundances and create a annotation for ComplexHeatmap package
abundance = as.data.frame(prop.table(table( ( prolif$Protein_snn_res.0.8 ) ) ) )
abundance$Var1 = paste0("Cluster ", abundance$Var1)
row_ha = rowAnnotation( `%cell` = anno_barplot(abundance$Freq) )
## scale expression matrix to max expression
expmatscale = apply(exp_mat, MARGIN = 1, FUN = function(X) (X - min(X))/diff(range(X)))
row.names(expmatscale) = gsub("Protein.", "Cluster ", row.names(expmatscale))
## define heatmap palette
colorpalette = brewer.pal(9, "YlGnBu") # define the colors for heatmap palette
## Draw the heatmap
hm = Heatmap(expmatscale,
             col = colorpalette,
             right_annotation = row_ha,
             row_km = 2)
pdf("~/tmp/heatmap.pdf", width = 10, height = 10)
print(hm)
dev.off()

#' Get heatmap of scaled average expression of ADT by activated clusters
## extract expression matrix
Idents(prolif) = prolif@meta.data$seurat_clusters
activated = subset(prolif, idents = c(1, 9, 15, 17 ,7, 8 ,12, 19, 3, 5, 0, 25))
exp_mat = AverageExpression(activated,
                            assays = "Protein",
                            features = ADT,
                            group.by = "ident")
exp_mat = as.data.frame(exp_mat$Protein)
names(exp_mat) = paste0("Cluster ", names(exp_mat))
row.names(exp_mat) = gsub( "-TotalSeqB", "", row.names(exp_mat) )
## extract cluster abundances and create a annotation for ComplexHeatmap package
abundance = as.data.frame(prop.table(table( ( activated$Protein_snn_res.0.8 ) ) ) ) 
abundance$Var1 = paste0("Cluster ", abundance$Var1)
abundance = abundance %>% filter(Freq != 0) 
row_ha = rowAnnotation( `%cell` = anno_barplot(abundance$Freq) )
## scale expression matrix to max expression
expmatscale = apply(exp_mat, MARGIN = 1, FUN = function(X) (X - min(X))/diff(range(X)))
row.names(expmatscale) = gsub("Protein.", "Cluster ", row.names(expmatscale))
## define heatmap palette
colorpalette = brewer.pal(9, "YlGnBu") # define the colors for heatmap palette
## Draw the heatmap
hm = Heatmap(expmatscale,
             col = colorpalette,
             right_annotation = row_ha )
pdf("~/tmp/heatmap_activated.pdf", width = 10, height = 10)
print(hm)
dev.off()


#' Save image of prolif object
saveRDS(prolif, file = "~/scrna_files/prolif_adtclust.RData")

## Add clustering according to cell cycle phase
#' Get cell phase
s.genes = cc.genes$s.genes
g2m.genes = cc.genes$g2m.genes
prolif = ScaleData(prolif, vars.to.regress = c("S.Score", "G2M.Score"), features = rownames(prolif))

#' Save image of prolif object
saveRDS(prolif, file = "~/scrna_files/prolif_adtclust_cyclecell.RData")
