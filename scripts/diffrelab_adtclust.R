#' Perform difference of relative abundance of clusters between AZM and CTRL groups in activated cells
#' Project: AZIMUT
#' Author: Nicolas Vallet

#' Require
library(Seurat)
library(tidyverse)

#' set paths 
path_to_rds  = "~/scrna_files"
path_to_save = "~/Git/azimutscrna/data"

#' Read RDS file with clustered
setwd(path_to_rds)
prolif = readRDS( "prolif_adtclust.RData" )

#' Get the prop table of cell in clusters among each condition
prop = prop.table( table(prolif@meta.data$sample_id, prolif@meta.data$seurat_clusters),1 )*100
prop = as.data.frame.matrix(prop)
names(prop) = paste0("Cluster",names(prop))
prop$group = NA
prop[grep("A", row.names(prop)) , "group"] = "AZM"
prop[grep("D", row.names(prop)) , "group"] = "DMSO"
prop[grep("N", row.names(prop)) , "group"] = "US"
prop$group = factor(prop$group, levels = c("US", "DMSO", "AZM") )
prop$sample = substr(row.names(prop), 1,2)

#' perform stats
res = data.frame(matrix( nrow = 1, ncol = 6) )
names(res) = c("var", "medAZM", "medDMSO", "medUS", "log2fcazm_dmso","pairedpval")
iteration  = 1
clusters   = names(prop)[ grep("Cluster", names(prop)) ]
list_plot  = list()
for(cluster in clusters) {

    ## get result table
    AZM  = prop %>% filter(group == "AZM")  %>% pull( all_of(cluster) )
    DMSO = prop %>% filter(group == "DMSO") %>% pull( all_of(cluster) )
    US   = prop %>% filter(group == "US")   %>% pull( all_of(cluster) )

    test = wilcox.test(AZM, DMSO, exact = FALSE, paired = TRUE)

    res[iteration , "var"    ] = cluster
    res[iteration , "medAZM" ] = median(AZM)
    res[iteration , "medDMSO"] = median(DMSO)
    res[iteration , "medUS"  ] = median(US)
    res[iteration , "log2fcazm_dmso"] = log( (median(AZM)/median(DMSO)), 2)
    res[iteration , "pairedpval"   ] = test$p.value

    iteration = iteration + 1

    ## get plots
    plot_i = prop %>%
    ggplot( aes_string(x = "group", y = cluster ) ) +
    geom_boxplot(data = prop, aes_string( fill = "group" ), color = "black", outlier.shape = NA, size = 0.5 ) +
    geom_line(data = prop, aes_string(group = "sample" ), linetype = "dotted", color = "black" ) +
    geom_point(shape = 21, color = "black", fill = "white", size = 3, ) +
    scale_fill_brewer(palette = "Dark2") +
    ggtitle(cluster) +
    ylab("%Cells") +
    theme_minimal() +
    theme(axis.title.x = element_blank(),
          axis.title.y = element_text(face = "bold", size = 12),
          axis.text    = element_text(face = "bold", size = 12, color = "black"),
          axis.line    = element_line(linetype = "solid"),
          axis.ticks   = element_line(linetype = "solid"),
          plot.title   = element_text(face = "bold", size = 12),
          legend.position = "none"
          )

    list_plot[[iteration]] = plot_i
    
}

#' save outputs
setwd( path_to_save )
## save plots
pdf("diffrelab_adtclust.pdf", height = 5, width = 5)
for( i in 1:length(list_plot) ) {
    print(list_plot[[i]])
}
dev.off()

## save result table with stats
write_csv2(res, "diffrelab_adtclust_stats.csv")
