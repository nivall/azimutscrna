#!/bin/sh
# runguix.sh
# This program runs the command line to generate GUIX environment

guix time-machine -C channels.scm -- environment -m manifest.scm -- R
