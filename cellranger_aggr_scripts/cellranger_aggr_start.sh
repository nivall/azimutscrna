#!/bin/bash
# Project: AZIMUT
# Author: Nicolas Vallet
# This script runs cellranger function to aggregate files identified in "aggr_libraries.csv" files and save it in the folder AGG_RUN
# Loop to create normalized and non-normalized result
# See create_csv_aggr to generate the aggr_libraries.csv file

normalization_list='mapped none'

echo "Cellranger aggretate will be run with aggr_libraries"

for norm in $normalization_list
do
    echo "Outputs are saved in AGG_RUN folder"
    echo "Normalization method is set as" $norm
    cellranger aggr --id=AGG_RUN_$norm --csv=aggr_libraries.csv --normalize=$norm
done
