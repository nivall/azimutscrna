#!/bin/bash
# Project AZIMUT, single cell analyses of CD3+ cells activated at 24h of culture (with AZM:A or DMSO=CTRL:D) or not activated (N) and harvested after 48h of culture
# Author: Nicolas Vallet
# This script runs a loop for iterative cellranger count pipelines
# Must be run from the "azimutfasq" directory in which ref libraries and feature will be available
# See create_csv_library.R to generate the library file and create_csv_featureref to generate the feature_ref.csv file

sample_list='18A 18D 18N 23A 23D 23N 30A 30D 30N 32A 32D 32N 35A 35D 35N 36A 36D 36N 37A 37D 37N 38A 38D 38N 39A 39D 39N 40A 40D 40N'

for sample in $sample_list
do
    cellranger count --id=$sample \
	       --libraries=library_$sample.csv \
	       --transcriptome=/home/nivall/yard/run_cellranger_count/refdata-cellranger-GRCh38-3.0.0 \
	       --feature-ref=feature_ref.csv \
	       --expect-cells=10000
done
