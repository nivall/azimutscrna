# Deciphering the transcriptomic impact of azithromycin on cultured T cells.

This repository stores scripts used for scRNA seq analysis. `FASTQ` and processed `HDF5` are stored in the GEO repository: GSE197658.

## Preprocessing

### Cellranger count pipe (`cellranger_count_scripts`)
Stores representative scripts to generate single cell RNA sequencing outputs from 1 sample (script to create feature and sample references and scripts to automatize cellranger count on multiple samples).

### Cellranger aggr pipe (`cellranger_aggr_scripts`)
Stores representative scripts to pool results from individual samples in a single output file with matrix.

## Analysis

### Computing environment (`guixconfig`)
To reproduce analyses, scripts can be ran in a [GNU Guix environment](https://guix.gnu.org/en/about/) with the `channels.scm` and `manifest.scm` files. Use the command below to run `R`.

`guix time-machine -C channels.scm -- environment -m manifest.scm -- R`

Additionnal informations on Guix environment can be found in the following ressources:
	1. [Our git repository](https://gitlab.com/nivall/guixreprodsci)
	2. [Guix HPC website](https://hpc.guix.info/)

### Source code of downstream analyses (`scripts`)
Stores the scripts used to perform analyses.

### Stored results (`res`)
Stores some of the result tables obtained with downstream analyses. Mostly used for plotting purposes.
